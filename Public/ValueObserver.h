// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "Delegates/DelegateSignatureImpl.inl"

namespace BIQ {

template<class T>
struct ValueObserver
{
public:
	typedef TBaseDelegate<void, T> FValueChanged;

	virtual ~ValueObserver() {}

	FValueChanged ValueChange;

	void Reset(T V = ValueObserver<T>::DefaultValue)
	{
		Previous = V;
	}

	void Update(T V)
	{
		if (false == IsEqual(Previous, V))
		{
			check(ValueChange.IsBound());
			Previous = V;
			ValueChange.Execute(V);
		}
	}

	virtual bool IsEqual(const T A, const T B)
	{
		check(false); // Has to be implemented!
		return false;
	}

	T Previous;

private:
	static T DefaultValue;
};

template<class T>
T ValueObserver<T>::DefaultValue{};

template<class T>
struct ValueObserverSimple : public ValueObserver<T>
{
	virtual bool IsEqual(const T A, const T B)
	{
		return A == B;
	}
};

}

#define OBSERVER_CALLBACK_NAME(Target) \
On##Target##Changed

#define DECLARE_OBSERVER_CALLBACK(Type, Target) \
UFUNCTION() \
void OBSERVER_CALLBACK_NAME(Target)(Type Value);

#define DECLARE_OBSERVER(Type, Target) \
	BIQ::ValueObserverSimple<Type> Target##Observer; \
	DECLARE_OBSERVER_CALLBACK(Type, Target)

#define DECLARE_OBSERVER_CUSTOM_EQUAL(Type, Target, EqualFunc) \
	class Custom##Type##Observer : public BIQ::ValueObserver<Type> { virtual bool IsEqual(const Type A, const Type B) override { return EqualFunc(A, B); } }; \
	Custom##Type##Observer Target##Observer; \
	DECLARE_OBSERVER_CALLBACK(Type, Target)

#define OBSERVER_CALLBACK_FULL_MEMBER_NAME(Class, Target) \
Class::OBSERVER_CALLBACK_NAME(Target)

#define DEFINE_OBSERVER(Class, Type, Target) \
void OBSERVER_CALLBACK_FULL_MEMBER_NAME(Class, Target)(Type Value)

#define UPDATE_OBSERVER(Target) \
{ Target##Observer.Update(Target); }

#define TRIGGER_OBSERVER_WITH_CURRENT(Target) \
{ Target##Observer.ValueChange.Execute(Target); }

#define RESET_OBSERVER(Target) \
{ Target##Observer.Reset(); }

#define RESET_OBSERVER_TO(Target, Value) \
{ Target##Observer.Reset(Value); }

#define RESET_OBSERVER_TO_FIELD(Target) \
RESET_OBSERVER_TO(Target, Target)

#define SETUP_OBSERVER(Class, Target) \
{ \
Target##Observer.ValueChange.Unbind(); \
Target##Observer.ValueChange.BindUObject(this, &OBSERVER_CALLBACK_FULL_MEMBER_NAME(Class, Target)); \
RESET_OBSERVER(Target); \
}