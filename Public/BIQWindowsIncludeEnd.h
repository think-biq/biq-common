// Copyright blurryroots innovation qanat OÜ. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.
/*! \file BIQWindowsIncludeEnd.h
    \brief Windows macro considerations. Used to BEFORE including additional windows API headers.

    ^^
*/

#include "Windows/PostWindowsApi.h"