// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "BIQCommonLog.h"
#include "BIQWindows.h"

#ifndef LOG_WINDOWS_ERRORCODE
#include "BIQStringify.h"
///
#define LOG_WINDOWS_ERRORCODE(Category, Level, CallState, Fmt, ...) \
{ \
    FString _ErrorCase_; BIQ::Windows::StringFromErrorCode(_ErrorCase_, CallState); \
    UE_LOG(Category, Level \
        , TEXT("In %s: Encountered Windows Errorcode (%d): %s // %s") \
        , CurrentFunctionName, CallState, *_ErrorCase_ \
        , *FString::Printf(Fmt, ##__VA_ARGS__) \
    ); \
}
#endif

