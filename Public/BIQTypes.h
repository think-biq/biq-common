// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"

#include "BIQTypes.generated.h"


/** Value indicating which RGB channel to consider. */
UENUM(BlueprintType)
enum class ERGBChannel : uint8
{
	Red,

	Green,

	Blue
};

/** Which approach to use to determine aspect ration. */
UENUM(BlueprintType)
enum class EAspectReferenceMode : uint8
{
	/** Uses the shape dimensions of the light. */
	Shape,

	/** Uses the dimensions of the media texture. */
	MediaTexture,

	/** Uses a manually specified value. */
	Manual
};

/** Which approach to use to determine the limits of an aspect ratio. */
UENUM(BlueprintType)
enum class EAspectAnchorMode : uint8
{
	/** Limits the result as to not exceet width of the medium. */
	Width,

	/** Limits the result as to not exceet height of the medium. */
	Height,

	/** Limits the result as to not exceet both width and height of the medium. */
	Both
};

/** Which approach to use to determine the limits of an aspect ratio. */
UENUM(BlueprintType)
enum class EAlphaMode : uint8
{
	/** Manually controlled by Opacity paramter. */
	None = 0,

	/** Manually controlled by Opacity paramter. */
	Manual,

	/** Borders will be interpreted as translucent. */
	Borders,

	/** Interprets the average color as opacity. */
	Color,

	/** Alpha texture. */
	Image,

	/** Alpha media texture / video. */
	Video
};

/** Explicit binary and operator. */
static uint8 operator &(const EAlphaMode& A, const EAlphaMode& B)
{
	uint8 FlagA = (uint8)A;
	uint8 FlagB = (uint8)B;
	uint8 HasFlagB = FlagA & FlagB;
	return HasFlagB;
}

/** Defines the mode to use to cut the borders. */
UENUM(BlueprintType)
enum class EBorderCutMode : uint8
{
	/** No border cut. */
	Raw,

	/** Precises cutting/letterboxing. */
	Letterbox,

	/** Smooth cutting with falloff. */
	Falloff
};