// Copyright blurryroots innovation qanat OÜ. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.
/*! \file BIQWindowsIncludeBegin.h
    \brief Windows macro considerations. Used to AFTER including additional windows API headers.

    ^^
*/

#include "CoreMinimal.h"

/// Redirect of UE internal text macro expansion.
#define UE_TEXT(Value) TEXT_PASTE(Value)

#include "Windows/PreWindowsApi.h"
#include "Windows/MinWindows.h"

/// Redirect of internal windows API text macro expansion.
#define WIN_TEXT(Value) __TEXT(Value)