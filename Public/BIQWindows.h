// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"

#if PLATFORM_WINDOWS
#include "BIQWindowsIncludeBegin.h"
#include "BIQWindowsIncludeEnd.h"
#endif

#include "BIQTypes.generated.h"

namespace BIQ {

#if PLATFORM_WINDOWS

namespace Windows {

BIQCOMMON_API
bool StringFromErrorCode(FString& ErrorMessage, DWORD ErrorCode);

}

#endif

}
