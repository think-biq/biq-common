// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TickInEditorPawn.generated.h"

UCLASS()
class BIQCOMMON_API ATickInEditorPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATickInEditorPawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BIQ|Common")
	bool bShouldTickInEditor;

	virtual bool ShouldTickIfViewportsOnly() const override
	{
		return bShouldTickInEditor;
	}

};
