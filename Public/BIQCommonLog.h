// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBIQCommon, Log, All);

#define COMMON_WARN(...) UE_LOG(LogBIQCommon, Warning, TEXT("%s"), ##__VA_ARGS__)
#define COMMON_ERROR(...) UE_LOG(LogBIQCommon, Error, TEXT("%s"), ##__VA_ARGS__)
