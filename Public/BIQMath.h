// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "BIQCommon.h"
#include "CoreMinimal.h"

namespace BIQ { namespace Math {

/** Calculates the multiplier depending on the angle and the length of barn doors (rect light). */
BIQCOMMON_API float CalculateExtensionLength(float DoorAngle, float DoorLength);

}}
