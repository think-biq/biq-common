// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Class.h"
#include "UObject/Object.h"
#include "BIQCommonLog.h"

#define StringifyRaw(Thing) #Thing
#define Stringify(Thing) TEXT(StringifyRaw(Thing))
#define FStringify(Thing) FString(Stringify(Thing))
#define StringifyBool(Thing) (Thing ? TEXT("TRUE") : TEXT("FALSE"))

#if defined(_MSC_VER)
	#if defined(_BIQ_QUOTE_FUNCSIG)
	#define CurrentFunctionName TEXT(__FUNCSIG__)
	#else
	#define CurrentFunctionName __FUNCSIG__
	#endif
#else
	#if defined(__GNUC__) || defined(__clang__)
	#define CurrentFunctionName __PRETTY_FUNCTION__
	#else
	#warning "Unknown platform, falling back to __FUNCTION__, disabling typename feature ..."
	#define CurrentFunctionName __FUNCTION__
	#define NoFunctionNameContext
	#endif
#endif


namespace BIQ { namespace Util {

static FString CleanupFunctionName(FString Name)
{
	const FString MemberToken(TEXT("::"));
	const int32 MemberTokenLength = MemberToken.Len();
	const FString ParamListToken(TEXT("("));

	int32 MemberTokenPos = Name.Find(MemberToken);
	int32 FunctionNameStartPos = MemberTokenPos + MemberTokenLength;
	int32 FunctionNameEndPos = Name.Find(ParamListToken, ESearchCase::IgnoreCase, ESearchDir::FromStart, FunctionNameStartPos);
	int32 FunctionNameLength = FunctionNameEndPos - FunctionNameStartPos;

	return Name.Mid(FunctionNameStartPos, FunctionNameLength);
}

}}

#define CurrentFunctionNameClean *BIQ::Util::CleanupFunctionName(FString(CurrentFunctionName))


namespace BIQ { namespace Util {

/** Defines the mode to use when fetching object name. */
enum class EObjectPathMode : uint8
{
	Full,

	Owner,

	Object
};

template <typename _Get_TypeName>
FString GetNativeTypeName(bool bStripContainerSpecifier = false)
{
	static FString TypeName;

	if (TypeName.IsEmpty())
	{
#if defined(NoFunctionNameContext)
		TypeName = TEXT("TYPE_NAME_NOT_SUPPORTED");
#else

	#if defined(_MSC_VER)
		const FString SignatureBegin = TEXT("GetNativeTypeName<");
		const FString SignatureEnd = TEXT(">");
	#else
		const FString SignatureBegin = TEXT("[_Get_TypeName = ");
		const FString SignatureEnd = TEXT("]");
	#endif
		const FString CurrentSignature = CurrentFunctionName;

		// Move cursor to the last character of the signature begin token.
		const int32 TypeNameStartPos = CurrentSignature.Find(SignatureBegin) + SignatureBegin.Len();
		// Move cursor to the signature end token.
		const int32 TypeNameEndPos = CurrentSignature.Find(*SignatureEnd, ESearchCase::IgnoreCase, ESearchDir::FromStart, TypeNameStartPos);
		// Calculate length of the type name by subtracting end of the signature from the start position.
		const int32 TypeNameLength = TypeNameEndPos - TypeNameStartPos;

		// Raw type name. Might include container specifier like 'class', 'struct' or 'enum'.
		TypeName = CurrentSignature.Mid(TypeNameStartPos, TypeNameLength);

	#if defined(_MSC_VER)
		if (bStripContainerSpecifier)
		{
			TypeName.RemoveFromStart(TEXT("class "));
			TypeName.RemoveFromStart(TEXT("struct "));
			TypeName.RemoveFromStart(TEXT("enum "));
		}
	#endif

#endif
	}

	return TypeName;
}

template<typename TEnum>
class EnumValueStringify
{
	TEnum Value;
	FString TypeName;
	UEnum* EnumType;

public:
	EnumValueStringify(TEnum Value)
		: Value(Value)
	{
		TypeName = GetNativeTypeName<TEnum>(true);
		check(0 < TypeName.Len());

		EnumType = FindObject<UEnum>(ANY_PACKAGE, *TypeName, true);
		check(EnumType);
	}

	FString ToString(bool bFullyQualified = false) const
	{
		FString ValueName = EnumType->GetNameStringByValue(static_cast<uint8>(Value));

		if (bFullyQualified)
		{
			ValueName = FString::Printf(TEXT("%s::%s")
				, *TypeName
				, *ValueName
			);
		}

		return ValueName;
	}

	TEnum ToValue() const
	{
		return Value;
	}
};

template<typename TEnum>
EnumValueStringify<TEnum> EnumValue(TEnum Value)
{
	return EnumValueStringify<TEnum>(Value);
}

BIQCOMMON_API bool
IsValid(const UObject* Object, bool bFast = false);

BIQCOMMON_API FString
GetDebugName(const UObject* Object, EObjectPathMode Mode = EObjectPathMode::Owner);

BIQCOMMON_API FString
GetDebugName(bool bValue);

}}