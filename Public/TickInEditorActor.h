// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TickInEditorActor.generated.h"

UCLASS(BlueprintType, Blueprintable, meta = (ChildCanTick))
class BIQCOMMON_API ATickInEditorActor : public AActor
{
public:
	GENERATED_BODY()
	
public:	
	ATickInEditorActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BIQ|Common")
	bool bShouldTickInEditor;

	virtual bool ShouldTickIfViewportsOnly() const override
	{
		return bShouldTickInEditor;
	}

};
