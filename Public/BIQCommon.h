// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "BIQCommonLog.h"

#ifdef IS_DEV_MODULE
// Nothing todo here
#else
#include "Modules/ModuleManager.h"

class FBIQCommonModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
#endif
