// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "CoreMinimal.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "DynamicMaterialHandle.generated.h"


/** Types of paramters in a dynamic material. */
UENUM(BlueprintType)
enum class EDynamicMaterialParamterType : uint8
{
	Scalar,

	Vector,

	Texture,

	VirtualTexture,

	Font,

	MaterialLayer,

	StaticSwitch,

	StaticComponentMask
};

USTRUCT(BlueprintType)
struct FDynamicMaterialHandle
{
	GENERATED_BODY()

	FDynamicMaterialHandle();

	FDynamicMaterialHandle(UMaterialInstanceDynamic* DynamicInstance);

	TMap<FName, EDynamicMaterialParamterType> ParameterTypeLookup;
	TMap<FName, bool> ParamterEditableLookup;

	UMaterialInstanceDynamic* DynMat;

};