// Copyright blurryroots innovation qanat OÜ. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

using UnrealBuildTool;

public class BIQCommon : ModuleRules
{
	public BIQCommon(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "MediaAssets",
            "MovieSceneTracks"
        });

		PrivateDependencyModuleNames.AddRange(new string[] {
            "MovieSceneTracks"
        });
	}
}
