// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#include "BIQStringify.h"
#include "Engine/Light.h"
#include "Engine/World.h"
#include "UObject/Object.h"
#include "Components/LightComponent.h"
#include "Runtime/MediaAssets/Public/MediaTexture.h"
#include "Engine/Texture2D.h"
#include "Materials/MaterialInterface.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/SpotLightComponent.h"
#include "Components/RectLightComponent.h"

namespace BIQ { namespace Util {

bool IsValid(const UObject* Object, bool bFast)
{
	return nullptr != Object
		&& (bFast
			? Object->IsValidLowLevelFast()
			: Object->IsValidLowLevel())
		&& false == Object->IsPendingKill()
		;
}

FString GetDebugName(const UObject* Object, EObjectPathMode Mode)
{
	FString Name(TEXT("_ERROR_GETTING_DEBUG_NAME_"));

	if (false == IsValid(Object, true))
	{
		Name = TEXT("NULL");
	}
	else
	{
		switch (Mode)
		{
		case EObjectPathMode::Full:
			Name = Object->GetFullName();
			break;
		case EObjectPathMode::Owner:
		{
			Name = Object->GetFullName();

			const int32 NameLength = Name.Len();
			const FString ActorHierachyStartToken = TEXT("PersistentLevel.");
			const int32 ActorHierachyStartTokenLength = ActorHierachyStartToken.Len();
			const int32 HierachyStartPos = Name.Find(ActorHierachyStartToken) + ActorHierachyStartTokenLength;
			const int32 HierachyLength = NameLength - HierachyStartPos;

			Name = Name.Mid(HierachyStartPos, HierachyLength);
		}
		break;
		case EObjectPathMode::Object:
			Name = Object->GetName();
			break;
		}
	}

	return Name;
}

FString GetDebugName(bool bValue)
{
	return bValue
		? TEXT("true")
		: TEXT("false")
		;
}

}}