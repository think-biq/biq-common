// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#include "BIQMath.h"
#include "Math/UnrealMathUtility.h" 

namespace BIQ { namespace Math {

float CalculateExtensionLength(float DoorAngle, float DoorLength)
{
	float DoorRadiants = FMath::DegreesToRadians(DoorAngle);
	float OppositeRadiants = FMath::DegreesToRadians(180.0f - (90.0f + DoorAngle));

	return (FMath::Sin(DoorRadiants) * DoorLength) / FMath::Sin(OppositeRadiants);
}

}}