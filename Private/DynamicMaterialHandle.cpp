// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#include "DynamicMaterialHandle.h"
#include "Materials/MaterialInterface.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstance.h"

FDynamicMaterialHandle::FDynamicMaterialHandle()
	: ParameterTypeLookup()
	, ParamterEditableLookup()
	, DynMat(nullptr)
{}

FDynamicMaterialHandle::FDynamicMaterialHandle(UMaterialInstanceDynamic* DynamicInstance)
	: FDynamicMaterialHandle()
{
	ParameterTypeLookup.Empty();
	ParamterEditableLookup.Empty();
	DynMat = DynamicInstance;

	TArray<FMaterialParameterInfo> Params;
	TArray<FGuid> ParamGuids;

	DynMat->GetAllScalarParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::Scalar);
		ParamterEditableLookup.Add(Info.Name, true);
	}

	DynMat->GetAllVectorParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::Vector);
		ParamterEditableLookup.Add(Info.Name, true);
	}

	DynMat->GetAllTextureParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::Texture);
		ParamterEditableLookup.Add(Info.Name, true);
	}

	DynMat->GetAllRuntimeVirtualTextureParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::VirtualTexture);
		ParamterEditableLookup.Add(Info.Name, true);
	}

	DynMat->GetAllFontParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::Font);
		ParamterEditableLookup.Add(Info.Name, true);
	}

#if WITH_EDITORONLY_DATA
#if 0
	DynMat->GetAllMaterialLayersParameterInfo(Params, ParamGuids);
#endif
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::MaterialLayer);
		ParamterEditableLookup.Add(Info.Name, false);
	}

	DynMat->GetAllStaticSwitchParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::StaticSwitch);
		ParamterEditableLookup.Add(Info.Name, false);
	}

	DynMat->GetAllStaticComponentMaskParameterInfo(Params, ParamGuids);
	for (auto Info : Params)
	{
		ParameterTypeLookup.Add(Info.Name, EDynamicMaterialParamterType::StaticComponentMask);
		ParamterEditableLookup.Add(Info.Name, false);
	}
#endif
}