// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.r

#include "TickInEditorPawn.h"

ATickInEditorPawn::ATickInEditorPawn()
	: bShouldTickInEditor(true)
{
	PrimaryActorTick.bCanEverTick = true;
}