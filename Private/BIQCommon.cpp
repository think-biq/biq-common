// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#include "BIQCommon.h"

#ifdef IS_DEV_MODULE
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, FBIQCommonModule, "BIQCommon");
#else

#define LOCTEXT_NAMESPACE "FBIQCommonModule"

void FBIQCommonModule::StartupModule()
{
	//
}

void FBIQCommonModule::ShutdownModule()
{
	//
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FBIQCommonModule, BIQCommon)
#endif