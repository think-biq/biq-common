// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#include "TickInEditorActor.h"

ATickInEditorActor::ATickInEditorActor()
	: bShouldTickInEditor(true)
{
	PrimaryActorTick.bCanEverTick = true;

}