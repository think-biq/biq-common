// Copyright blurryroots innovation qanat O�. All Rights Reserved.
// Visit http://think-biq.com/projectionist for more information.

#pragma once

#include "BIQWindows.h"
#if PLATFORM_WINDOWS
#include "BIQWindowsLog.h"
#endif

namespace BIQ {

#if PLATFORM_WINDOWS

namespace Windows {

bool StringFromErrorCode(FString& ErrorMessage, DWORD ErrorCode)
{
    bool bSuccess = false;
    WCHAR Buffer[512];

    DWORD cchMsg = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,  /* (not used with FORMAT_MESSAGE_FROM_SYSTEM) */
        ErrorCode,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        Buffer,
        512,
        NULL
    );

    if (cchMsg > 0)
    {
        ErrorMessage = FString(Buffer);
        bSuccess = true;
    }

    return bSuccess;
}

}

#endif

}
